import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import Home from './components/pages/Home';
import Register from './components/pages/Register';


class App extends Component {
	render() {
		return (
			<BrowserRouter>
					<Switch>
						<Route exact path='/'component={Home} />
						<Route path='/register' component={Register} />
					</Switch>
			</BrowserRouter>
		);
	}
}

export default App;


