/* eslint no-mixed-operators: 0 */


import React from 'react';
import InputMask from 'react-input-mask';
import { defaultProps } from 'prop-types';

class Field extends React.Component {

	state = {
		error: false,
		touched: false,
		valid: false,
		dirty: false
	}

	onFocusHandler = () => {
		if(!this.state.touched){
			this.setState({
				touched: true
			});	
		}
	}

	onBlurHandler = (e) => {
		if(!this.state.dirty){
			this.setState({
				dirty: true
			});
			if(!this.props.invalid){
				this.setState({
					valid: true
				});
			}
		} else {
			if(!this.props.invalid){
				this.setState({
					valid: true
				});
			}
		}
	}

	onChangeHandler = (e) => {
		if(this.state.dirty && !this.props.invalid){
			this.setState({
				valid: true
			});
		}

		this.props.changehandler(e);
	}


	render(){


		if(this.props.mask.length > 0){

			return (
				<div className={"form-group" + (this.props.invalid && this.state.dirty || this.props.invalid && this.props.submitted ? ' has-error' : '')}>
					<label htmlFor={this.props.id}>{this.props.label}</label>
					<InputMask 
						className={"form-control" + (this.props.invalid && this.state.dirty ? ' error' : '') + (this.state.valid ? ' valid' : '') + (this.props.helpmessage.length > 0 ? ' has-help' : '')}
						name={this.props.id} 
						id={this.props.id}
						mask={this.props.mask} 
						maskChar="_"
						placeholder={this.props.placeholder}
						type={this.props.type}
						value={this.props.value}
						onChange={this.onChangeHandler}
						onBlur={this.onBlurHandler}
						onFocus={this.onFocusHandler}
					/>
					{ this.props.helpmessage.length > 0 && <span className="helper">{this.props.helpmessage}</span> }
					{ this.props.invalid && this.state.dirty || this.props.invalid && this.props.submitted ? <span className="help-block form-error">{this.props.errormessage}</span> : '' }
				</div>

			);

		} else {

			return (

				<div className={"form-group" + (this.props.invalid && this.state.dirty || this.props.invalid && this.props.submitted ? ' has-error' : '')}>
					<label htmlFor={this.props.id}>{this.props.label}</label>
					<input 
						className={"form-control" + (this.props.invalid && this.state.dirty ? ' error' : '') + (this.state.valid ? ' valid' : '') + (this.props.helpmessage.length > 0 ? ' has-help' : '')}
						name={this.props.id} 
						id={this.props.id}
						placeholder={this.props.placeholder}
						value={this.props.value}
						onChange={this.onChangeHandler}
						onBlur={this.onBlurHandler}
						onFocus={this.onFocusHandler}
					/>
					{ this.props.helpmessage.length > 0 && <span className="helper">{this.props.helpmessage}</span> }
					{ this.props.invalid && this.state.dirty || this.props.invalid && this.props.submitted ? <span className="help-block form-error">{this.props.errormessage}</span> : '' }
				</div>

			);

		}
		
	}
}


// Specifies the default values for props:
Field.defaultProps = {
  label: 'Field Label',
  id: 'field_id_name',
  placeholder: '',
  errormessage: '',
  helpmessage: '',
  mask: '',
  type: 'text'
};


export default Field;