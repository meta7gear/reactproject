/* eslint no-useless-escape: 0 */

import React, { Component } from 'react';
import FormValidator from './FormValidator';
import Field from '../form/Field';

class Form extends Component {
  constructor(props) {
    super(props);

    this.validator = new FormValidator([
      { 
        field: 'first_name', 
        method: 'isEmpty', 
        validWhen: false, 
        message: 'Введите ваше имя' 
      },
      { 
        field: 'last_name', 
        method: 'isEmpty', 
        validWhen: false, 
        message: 'Введите вашу фамилию' 
      },
      { 
        field: 'middle_name', 
        method: 'isEmpty', 
        validWhen: false, 
        message: 'Введите ваше отчество' 
      },
      { 
        field: 'email', 
        method: 'isEmpty', 
        validWhen: false, 
        message: 'Введите ваш email'
      },
      { 
        field: 'email',
        method: 'isEmail', 
        validWhen: true, 
        message: 'Введите ваш email'
      },
      { 
        field: 'phone', 
        method: 'isEmpty',
        validWhen: false, 
        message: 'Введите ваш номер телефона'
      },
      {
        field: 'phone', 
        method: 'matches',
        args: [/^(\+7\([489]\d{2}\))(\d{3})\-(\d{2})\-(\d{2})$/],
        validWhen: true, 
        message: 'Введите ваш номер телефона'
      },
      { 
        field: 'id_number', 
        method: 'isEmpty',
        validWhen: false, 
        message: 'Введите ваш номер паспорта'
      },
      {
        field: 'id_number', 
        method: 'matches',
        args: [/^(\d{4}\s\d{6})$/],
        validWhen: true, 
        message: 'Введите ваш номер паспорта'
      },
      
    ]);

    this.state = {

      first_name: '',
      last_name: '',
      middle_name: '',
      email: '',
      phone: '',
      id_number: '',

      validation: this.validator.valid(),
    }

    this.submitted = false;
  }

  handleInputChange = event => {
    event.preventDefault();

    this.setState({
      [event.target.name]: event.target.value,
    });
  }
    
  handleFormSubmit = event => {
    event.preventDefault();

    const validation = this.validator.validate(this.state);
    this.setState({ validation });
    this.submitted = true;

    if (validation.isValid) {
      const formData = {
        first_name: this.state.first_name,
        last_name: this.state.last_name,
        middle_name: this.state.middle_name,
        email: this.state.email,
        phone: this.state.phone,
        id_number: this.state.id_number
      }
      this.props.submithandler(formData);
    }
  }

  render() {
    let validation = true ? // IF TRUE then check validity every time we render
                      this.validator.validate(this.state) :   
                      this.state.validation                   // otherwise just use what's in state

    return (

      <form method="POST" id="application-form" onSubmit={this.handleFormSubmit}>
            <div className="row">
              <div className="col-xl-6">
                <Field 
                  label="Имя"
                  id="first_name"
                  placeholder="Введите ваше имя"
                  errormessage="Введите ваше имя"
                  invalid={validation.first_name.isInvalid}
                  submitted={this.submitted}
                  value={this.state.first_name}
                  changehandler={this.handleInputChange}
                />

              </div>

              <div className="col-xl-6">
                <Field 
                  label="Фамилия"
                  id="last_name"
                  placeholder="Введите вашу фамилию"
                  errormessage="Введите вашу фамилию"
                  invalid={validation.last_name.isInvalid}
                  submitted={this.submitted}
                  value={this.state.last_name}
                  changehandler={this.handleInputChange}
                />
              </div>
            </div>
            <div className="row">
              <div className="col-xl-6">
                <Field 
                  label="Отчество"
                  id="middle_name"
                  placeholder="Введите ваше отчество"
                  errormessage="Введите ваше отчество"
                  invalid={validation.middle_name.isInvalid}
                  submitted={this.submitted}
                  value={this.state.middle_name}
                  changehandler={this.handleInputChange}
                />
              </div>
              <div className="col-xl-6">
                <Field 
                  label="Email"
                  id="email"
                  placeholder="example@email.com"
                  errormessage="Введите ваш email"
                  type="email"
                  invalid={validation.email.isInvalid}
                  submitted={this.submitted}
                  value={this.state.email}
                  changehandler={this.handleInputChange}
                />
              </div>
            </div>
            <div className="row">
              <div className="col-xl-6">
                <Field 
                  label="Телефон"
                  id="phone"
                  placeholder="+7(999)999-99-99"
                  errormessage="Введите ваш номер телефона"
                  helpmessage="Формат: +7(999)999-99-99"
                  mask="+7(999)999-99-99"
                  type="tel"
                  invalid={validation.phone.isInvalid}
                  submitted={this.submitted}
                  value={this.state.phone}
                  changehandler={this.handleInputChange}
                />
              </div>
              <div className="col-xl-6">
                <Field 
                  label="Номер паспорта"
                  id="id_number"
                  placeholder="Пример: 1234 123456"
                  errormessage="Введите ваш номер паспорта"
                  helpmessage="Формат: 1234 123456"
                  mask="9999 999999"
                  type="tel"
                  invalid={validation.id_number.isInvalid}
                  submitted={this.submitted}
                  value={this.state.id_number}
                  changehandler={this.handleInputChange}
                />
              </div>
            </div>
            
            <div className="form-group">
              <button type="submit" className="btn btn-cta btn-lg">{ this.props.sending ? 'Загрузка' : 'Оформить' } &nbsp; &raquo;</button>
            </div>
        
          </form>
    )
  }
}
export default Form;