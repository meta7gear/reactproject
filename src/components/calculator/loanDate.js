let dueDate = new Date();
dueDate.setTime(dueDate.getTime() + 14 * 24 * 60 * 60 * 1000);

export const firstDate = dueDate.getDate() + '.' + (dueDate.getMonth() + 1) + '.' + dueDate.getFullYear();