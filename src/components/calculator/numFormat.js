import wNumb from 'wnumb';

export const moneyForm = wNumb({
	thousand: ' ',
	decimal: ',',
	decimals: 0
});

export const moneyFormD = wNumb({
	thousand: ' ',
	decimal: ',',
	decimals: 2
});