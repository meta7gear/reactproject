import React from 'react';
import noUiSlider from 'nouislider';
//import wNumb from 'wnumb';

class Slider extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			start: this.props.start,
			connect: this.props.connect,
			step: this.props.step,
			range: this.props.range,
		}
	}
	componentDidMount() {
		let slider = document.getElementById(this.props.id);
		noUiSlider.create(slider, {
			start: this.state.start,
			connect: this.state.connect,
        	step: this.state.step,
			range: this.state.range,
			//format: wNumb({ decimals: 0, thousand: ' ' })
		});
		slider.noUiSlider.on('update', this.props.onChange);
	}


	onChangePage = (event, item) => {
		this.props.onChange(event);
	}


	render() {
		return (
			<div>
				<div className="slider-value-container clearfix">
				</div>
				<div id={this.props.id} className="slider"></div>
			</div>
		);
	}
 }

 export default Slider;