import React from 'react';
import ScrollableAnchor from 'react-scrollable-anchor';

import Hero from '../sections/Hero';
import HowItWorks from '../sections/HowItWorks';
import Why from '../sections/Why';
import FAQ from '../sections/FAQ';
import ActionBar from '../sections/ActionBar';
import Footer from '../sections/Footer';


const Home = () => {
	return (
		<div>
			<Hero />
			<ScrollableAnchor id={'how-it-works'}>
				<div><HowItWorks /></div>
			</ScrollableAnchor>
			<ScrollableAnchor id={'why'}>
				<div><Why /></div>
			</ScrollableAnchor>
			<ScrollableAnchor id={'faq'}>
				<div><FAQ /></div>
			</ScrollableAnchor>
			<ActionBar />
			<Footer />
		</div>
	);
}

export default Home;