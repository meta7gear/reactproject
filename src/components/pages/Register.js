/* eslint no-useless-escape: 0 */

import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Footer from '../sections/Footer';
import Calculator from '../sections/Calculator';
import LoanDetails from '../sections/LoanDetails';
import Form from '../form/Form';

class Register extends React.Component {

	state = {
		editing: false,
		sending: false,
	}

	componentDidMount(){
		window.scrollTo(0,0);
		
	}

	onEditHandler = () => {
		this.setState({
			editing: true
		});
	}

	closeCalculatorHandler = () => {
		this.setState({
			editing: false
		});
	}

	postFormData = (formData) => {
		const { loan } = this.props;
		const data = {
			...formData,
			loan_amount: loan.loanAmount,
			loan_term: loan.loanTerm
		}

		this.setState({
			sending: true
		});

		console.log(data);

		//Axios Post here...

	}

	render(){

		return (
			<div>
				<header className="darkgrey-bg txt-white">
					<div className="container">
						<div className="row align-items-center">
							<div className="col-3 d-block d-lg-none">
								
							</div>
							<div className="col-6 col-lg-2">
								<Link to="/" className="txt-white bold">
									Logo
								</Link>
							</div>
							<div className="col-3 col-lg-10">
								<nav className="d-none d-lg-block">
									<ul>
										<li><Link to="/">&laquo; Назад</Link></li>
									</ul>
								</nav>
								<div className="nav-icon d-block d-lg-none">
									<span></span>
									<span></span>
									<span></span>
								</div>
							</div>
						</div>
					</div>
				</header>

				<main className="bump-top-md bump-bottom-md">
					<div className="container">

						<h1 className="bump-bottom-md hu">Форма регистрации</h1>
						<div className="row">
							<div className="col-lg-8 bump-bottom-sm">
								<div className="row bump-bottom-sm">
									<div className="col-lg-4">
										<div className="step step-icon-slider">
											<strong>1.</strong> Выберите кредит
										</div>
									</div>
									<div className="col-lg-4 text-center">
										<div className="step active step-icon-form">
											<strong>2.</strong> Регистрация
										</div>
									</div>
									<div className="col-lg-4 text-right">
										<div className="step final step-icon-coins">
											<strong>3.</strong> Получить деньги
										</div>
									</div>
								</div>

								<Form submithandler={this.postFormData} sending={this.state.sending} />

							</div>

							<div className="col-lg-4 bump-bottom-sm">
							{ !this.state.editing ? <LoanDetails onEdit={this.onEditHandler} /> : <Calculator editing={true} closeCalculator={this.closeCalculatorHandler} /> }
								
							</div>
						</div>
					</div>
				</main>

				<section id="contact-bar" className="grey-bg pad-top-sm">
					<div className="container">
						<div className="row">
							<div className="col-sm-9 col-lg-6 bump-bottom-sm">
								<h3 className="semi-bold">Нужна помощь?</h3>
								<p className="p16">Наша команда обслуживания клиентов поможет!</p>
							</div>
							<div className="col-12 col-lg-4 bump-bottom-sm">
								<h3 className="semi-bold"> +7 (999) 999-99-99</h3>
								<div className="p16"><i className="far fa-clock fa-fw"></i> Ежедневно 07:00 - 20:00</div>
							</div>
							<div className="col-lg-2 d-none d-sm-block">
								<div id="cb-woman"></div>
							</div>
						</div>
					</div>
				</section>
				<Footer />
			</div>
		);
	}
}

const mapStateToProps = (state) => {
  return {
    loan: state.loan
  }
}

export default connect(mapStateToProps)(Register);