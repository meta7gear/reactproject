import React from 'react';
import { Link } from 'react-router-dom';

const Why = () => {
	return (
		<section id="preimushchestva" className="pad-top-md pad-bottom-md">
			<div className="container">
				<h2 className="bump-bottom-md text-center">Преимущества</h2>
				<div className="row justify-content-center">
					<div className="col-lg-8 bump-bottom-sm">
						<ul className="feature-list ">
							<li className="row align-items-center"><div className="col">100% онлайн подача заявки</div>
								<div className="col-2 text-right">
									<span className="fa-stack ">
		  								<i className="fas fa-circle fa-stack-2x txt-dark-green"></i>
		  								<i className="fas fa-check fa-stack-1x fa-xs txt-white"></i>
									</span>
								</div>
							</li>
							<li className="row align-items-center"><div className="col">Одобрение и выдача займа менее чем за 1 час</div>
								<div className="col-2 text-right">
									<span className="fa-stack ">
		  								<i className="fas fa-circle fa-stack-2x txt-dark-green"></i>
		  								<i className="fas fa-check fa-stack-1x fa-xs txt-white"></i>
									</span>
								</div>
							</li>
							<li className="row align-items-center"><div className="col">Без залога и поручителей</div>
								<div className="col-2 text-right">
									<span className="fa-stack ">
		  								<i className="fas fa-circle fa-stack-2x txt-dark-green"></i>
		  								<i className="fas fa-check fa-stack-1x fa-xs txt-white"></i>
									</span>
								</div>
							</li>
							<li className="row align-items-center"><div className="col">Нет скрытых комиссий</div>
								<div className="col-2 text-right">
									<span className="fa-stack ">
		  								<i className="fas fa-circle fa-stack-2x txt-dark-green"></i>
		  								<i className="fas fa-check fa-stack-1x fa-xs txt-white"></i>
									</span>
								</div>
							</li>
							<li className="row align-items-center"><div className="col">Большая сумма без лишних справок</div>
								<div className="col-2 text-right">
									<span className="fa-stack ">
		  								<i className="fas fa-circle fa-stack-2x txt-dark-green"></i>
		  								<i className="fas fa-check fa-stack-1x fa-xs txt-white"></i>
									</span>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div className="text-center bump-top-sm">
					<Link to="/register" className="btn btn-cta btn-lg apply-button" id="apply-button-home-how">Подать заявку</Link>
				</div>
			</div>
		</section>
	);
}

export default Why;