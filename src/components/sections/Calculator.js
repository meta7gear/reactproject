import React from 'react';
import { Link } from 'react-router-dom';
import Slider from '../calculator/Slider';

import { connect } from 'react-redux';
import { moneyForm, moneyFormD } from '../calculator/numFormat';
import { firstDate } from '../calculator/loanDate';


class Calculator extends React.Component {

	componentWillMount(){
		if(this.props.editing){
			this.setState({
				editing: true
			});
		}
	}

	state = {
		editing: false
	}

	handleAmountChange = (e) => {
		this.calculateRepayment(e[0], this.props.loan.loanTerm);
	}

	handleTermChange = (e) => {
		this.calculateRepayment(this.props.loan.loanAmount, e[0]);
	}

	onChangeHandler = (e) => {
		console.log(e);
	}

	save = () => {
		this.setState({
			editing: false
		});
		this.props.closeCalculator();
	}

	calculateRepayment = (amount, term) => {
		const loan_amount = parseFloat(amount);
		const loan_term = parseFloat(term);
		const annual_rate = loan_term < 24 ? 328.50 : 182.50;
		const daily_rate = annual_rate / 365;
		const periods = loan_term / 2;
		const rate = (annual_rate / periods) / 100;
		const fee = 0;

		//Calculate the repayment amount
		const x = Math.pow(1 + rate, periods);
		const payment_amount = (loan_amount * x * rate) / (x - 1);
		
		const newLoan = {
			loanAmount: loan_amount,
			loanTerm: loan_term,
			annualRate: annual_rate,
			dailyRate: daily_rate,
			periods: periods,
			rate: rate,
			fee: fee,
			repayment: payment_amount, 
			firstDate: firstDate
		};

		this.props.updateLoan(newLoan);
	}

	render() {
		
		const { loan } = this.props;
		
		return (
			<form method="POST" action="apply.html">
				<div className="calculator-top">
					<div className="bump-bottom-sm p25 semi-bold d-none d-lg-block">Сумма перевода</div>
					<div className="d-flex align-items-center bump-bottom-xs">
						<div>
							<label className="noUi-label">Выберите сумму<span className="d-inline d-lg-none slider-value-unit"> <small>(₽)</small></span></label>
						</div>
						<div className="ml-auto text-right">
							<span className="slider-value">
								<input type="tel" className="slider-value-amount" id="loanamount_text" onChange={this.onChangeHandler} value={moneyForm.to(parseFloat(loan.loanAmount))} /> <span className="slider-value-unit d-none d-lg-inline"><span className="rub">₽</span></span>
							</span>
						</div>
					</div>
					<div className="bump-bottom-none">
						<Slider
							id="amountSlider"
							onChange={this.handleAmountChange}
							range={{ min: 10000, max: 100000 }}
							start={loan.loanAmount}
							connect={[true, false]}
							step={1000}
						/>
					</div>
					<div className="d-flex bump-bottom-sm txt-fade-green">
						<div>
							<span className="p12">30 000 <span className="rub">₽</span></span>
						</div>
						<div className="ml-auto text-right">
							<span className="p12">100 000 <span className="rub">₽</span></span>
						</div>
					</div>
					<div className="d-flex align-items-center bump-bottom-xs">
						<div>
							<label className="noUi-label">Выберите срок<span className="slider-value-unit d-block d-lg-none"> <small>(Недели)</small></span></label>
						</div>
						<div className="ml-auto text-right">
							<span className="slider-value">
								<input type="tel" className="slider-value-amount" id="loanterm_text" onChange={this.onChangeHandler}  value={moneyForm.to(parseFloat(loan.loanTerm))} style={{width: 40}} /> <span className="slider-value-unit d-none d-lg-inline">Нед.</span>
							</span>
						</div>
					</div>
					<div>
						<Slider
							id="termSlider"
							onChange={this.handleTermChange}
							range={{ min: 12, max: 52 }}
							start={loan.loanTerm}
							connect={[true, false]}
							step={2}
						/>
					</div>
					<div className="d-flex bump-bottom-sm txt-fade-green">
						<div>
							<span className="p12">12 Недель</span>
						</div>
						<div className="ml-auto text-right">
							<span className="p12">52 Недель</span>
						</div>
					</div>
					<div className="text-center">
						{ !this.state.editing ?
							<Link to="/register" className="btn btn-cta btn-lg apply-button apply-button-calculator" id="apply-button-home-main">Получить деньги</Link>
						:
							<button className="btn btn-white" onClick={this.save}><i className="fas fa-check fa-fw"></i> подтвердить</button>
						}
					</div>
				</div>

				<div className={ this.state.editing ? "calculator-bottom bump-bottom-sm grey-bg" : "calculator-bottom bump-bottom-sm"}>
					<div className="loan-detail-row d-flex">
						<div>
							<span className="grey p15">Дата первого платажа</span>
						</div>
						<div className="ml-auto text-right">
							<span className="grey p15"><span className="loan-firstdate-display">{loan.firstDate}</span></span>
						</div>
					</div>
					<div className="loan-detail-row d-flex">
						<div>
							<span className="grey p15">Процентная ставка</span>
						</div>
						<div className="ml-auto text-right">
							<span className="grey p15"><span className="loan-dpr-display">{moneyFormD.to(loan.dailyRate)}</span> %</span>
						</div>
					</div>
					<div className="loan-detail-row d-flex">
						<div>
							<span className="grey p15">Комиссия</span>
						</div>
						<div className="ml-auto text-right">
							<span className="grey p15"><span className="loan-fee-display">{moneyFormD.to(loan.fee)}</span> <span className="rub">₽</span></span>
						</div>
					</div>

					<div className="loan-detail-row d-flex semi-bold">
						<div>
							<span className="p15">Платеж раз в 2 недели</span>
						</div>
						<div className="ml-auto text-right">
							<span className="repayment-display">{moneyForm.to(loan.repayment)}</span> <span className="rub">₽</span>
						</div>
					</div>
				</div>
			</form>
		);
	}
}

const mapStateToProps = (state) => {
  return {
    loan: state.loan
  }
}

const mapDispatchToProps = (dispatch) => {
	return {
		updateLoan: (loan) => dispatch({ type: 'UPDATE_LOAN', loan: loan })
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Calculator);