import React from 'react';
import { Link } from 'react-router-dom';

const HowItWorks = () => {
	return (
		<section id="kak-poluchit" className="pad-top-md pad-bottom-md light-green-bg txt-white">
			<div className="container">
				<h2 className="text-center bump-bottom-md txt-white">Как получить деньги</h2>
				<div className="row">
					<div className="col-lg-4 bump-bottom-sm text-center">
						<div className="hta-icon">
							<object data="images/hta_icon1.svg" type="image/svg+xml">
							</object>
						</div>
						<h5 className="semi-bold bump-bottom-sm">1. Заполните анкету<br />на сайте</h5>
						<p>Вам понадобятся паспорт, мобильный телефон и адрес email.</p>
					</div>
					<div className="col-lg-4 bump-bottom-sm text-center">
						<div className="hta-icon">
							<object data="images/hta_icon2.svg" type="image/svg+xml">
							</object>
						</div>
						<h5 className="semi-bold bump-bottom-sm">2. Решение об<br />одобрении онлайн</h5>
						<p>Мы проверим вашу заявку и отправим вам мгновенное решение.</p>
					</div>
					<div className="col-lg-4 bump-bottom-sm text-center">
						<div className="hta-icon">
							<object data="images/hta_icon3.svg" type="image/svg+xml">
							</object>
						</div>
						<h5 className="semi-bold bump-bottom-sm">3. Офис рядом с вашим домом<br />или местом работы</h5>
						<p>Одобренная сумма будет перечислена на вашу карту. Необходимо посетить офис.</p>
					</div>
				</div>
				<div className="text-center bump-top-sm">
					<Link to="/register" className="btn btn-cta btn-lg apply-button" id="apply-button-home-how">Подать заявку</Link>
				</div>
			</div>
		</section>
	);
}

export default HowItWorks;