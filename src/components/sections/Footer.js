import React from 'react';
import { Link } from 'react-router-dom';

const Footer = () => {
	return (
		<footer className="pad-top-md pad-bottom-md darkgrey-bg txt-white">
			<div className="container">
				<div className="row">
					<div className="col-lg-4 bump-bottom-sm align-self-center">
						<Link to="/" className="txt-white bold">
							Logo
						</Link>
					</div>
					<div className="col-lg-4 bump-bottom-sm">
						<h5 className="bump-bottom-sm"><Link to="/register">Подать заявку</Link></h5>
						<div className="p14">
							<a href="http://example.com">О компании</a><br />
							<a href="http://example.com">Контакты</a><br />
							<a href="http://example.com">Войти в личный кабинет</a>
						</div>
					</div>
					<div className="col-lg-4 bump-bottom-sm">
						<h5 className="bump-bottom-sm"><a href="http://example.com">Контакты</a></h5>
						<div className="p14">
							<i className="fas fa-phone fa-fw"></i> <a href="tel:+79999999999">+7 (999) 999-99-99</a><br />
							<i className="fas fa-clock fa-fw"></i> Ежедневно 07:00 - 20:00<br />
							<i className="fas fa-paper-plane fa-fw"></i> <a href="mailto:info@example.ru">info@example.ru</a><br />
						</div>
					</div>				
				</div>
			</div>
		</footer>
	);
}

export default Footer;