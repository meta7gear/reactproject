import React from 'react';
import { connect } from 'react-redux';
import { moneyForm, moneyFormD } from '../calculator/numFormat';

class LoanDetails extends React.Component {


	onEditLoan = () => {
		this.props.onEdit();
	}

	render(){

		const { loan } = this.props;

		return (

			<div className="box light-green-bg txt-white">
				<div className="details-box-header">
					<h3 className="bump-bottom-sm bump-top-none txt-white">Заявка на кредит</h3>
				</div>
				<div className="details-box-body bump-bottom-xs">

					<div className="d-flex loan-detail-row">
						<div>
							<span className="grey p14">Сумма</span>
						</div>
						<div className="ml-auto text-right">
							<span className="grey p14"><span>{moneyForm.to(loan.loanAmount)}</span> ₽</span>
						</div>
					</div>

					<div className="d-flex loan-detail-row">
						<div>
							<span className="grey p14">Срок</span>
						</div>
						<div className="ml-auto text-right">
							<span className="grey p14"><span>{loan.loanTerm}</span> недели</span>
						</div>
					</div>

					<div className="d-flex loan-detail-row">
						<div>
							<span className="grey p14">Дата первого платежа</span>
						</div>
						<div className="ml-auto text-right">
							<span className="grey p14">{loan.firstDate}</span>
						</div>
					</div>

					<div className="d-flex loan-detail-row">
						<div>
							<span className="grey p14">Процентная ставка</span>
						</div>
						<div className="ml-auto text-right">
							<span className="grey p14 ">{moneyFormD.to(loan.annualRate)} %</span>
						</div>
					</div>

					<div className="d-flex loan-detail-row">
						<div>
							<span className="grey p14">Комиссия</span>
						</div>
						<div className="ml-auto text-right">
							<span className="grey p14">{moneyFormD.to(loan.fee)} ₽</span>
						</div>
					</div>							
							
					<div className="d-flex loan-detail-row ">
						<div>
							<span className="grey p14 bold">Платеж раз в 2 недели</span>
						</div>
						<div className="ml-auto text-right">
							<span className="grey p14 bold">{moneyForm.to(loan.repayment)}  ₽</span>
						</div>
					</div>

					<div className="text-center bump-top-sm">
						<button className="btn btn-white" onClick={this.onEditLoan}><i className="fas fa-pencil-alt fa-fw"></i> Изменить</button>
					</div>

				</div>
			</div>

		);
	}
}


const mapStateToProps = (state) => {
  return {
    loan: state.loan
  }
}

export default connect(mapStateToProps)(LoanDetails);