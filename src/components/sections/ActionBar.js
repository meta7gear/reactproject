import React from 'react';
import { Link } from 'react-router-dom';


const ActionBar = () => {
	return (
		<section id="bottom-cta" className="pad-top-md pad-bottom-md light-green-bg">
			<div className="container">
				<h3 className="text-center bump-bottom-md txt-white">Заполните анкету и получите деньги</h3>
				<div className="text-center">
					<Link to="/register" className="btn btn-cta btn-lg apply-button" id="apply-button-home-how">Подать заявку</Link>
				</div>
			</div>
		</section>
	);
}

export default ActionBar;