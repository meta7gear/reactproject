import React from 'react';

const FAQ = () => {
	return (
		<section id="frequently-asked-questions" className="pad-top-md pad-bottom-md grey-bg">
			<div className="container">
				 <h2 className="text-center bump-bottom-md">Часто задаваемые вопросы</h2>
				 <div className="row justify-content-center">
				 	<div className="col-lg-11">
						<div className="row justify-content-center">
							<div className="col-lg-6 question">
								<div className="row">
									<div className="col-md-2 d-none d-md-block">
										<img src="images/icon_faq.png" className="img-fluid" alt="(?)" />
									</div>
									<div className="col-md-10">
										<h4 className="question-title active">Какие условия получения потребительского займа в company?</h4>
										<div className="question-answer"><p>Мы выдаём деньги без поручителей, залога и лишних справок. Company - это надёжный и быстрый аналог банковского кредита.</p></div>
									</div>
								</div>
							</div>
							<div className="col-lg-6 question">
								<div className="row">
									<div className="col-md-2 d-none d-md-block">
										<img src="images/icon_faq.png" className="img-fluid" alt="(?)" />
									</div>
									<div className="col-md-10">
										<h4 className="question-title">Как получить?</h4>
										<div className="question-answer"><p>Заполните онлайн-анкету и дождитесь одобрения заявки. Деньги переводятся на банковскую карту или счёт после визита в офис.</p></div>
									</div>
								</div>
							</div>
						</div>
						<div className="row justify-content-center">
							<div className="col-lg-6 question">
								<div className="row">
									<div className="col-md-2 d-none d-md-block">
										<img src="images/icon_faq.png" className="img-fluid" alt="(?)" />
									</div>
									<div className="col-md-10">
										<h4 className="question-title">Как погасить?</h4>
										<div className="question-answer"><p>Вносите регулярный платёж — после последней оплаты займ будет погашен. Также доступно досрочное погашение без комиссий и штрафов.</p></div>
									</div>
								</div>
							</div>
							<div className="col-lg-6 question">
								<div className="row">
									<div className="col-md-2 d-none d-md-block">
										<img src="images/icon_faq.png" className="img-fluid" alt="(?)" />
									</div>
									<div className="col-md-10">
										<h4 className="question-title">Как быстро я получу деньги?</h4>
										<div className="question-answer"><p>Заполнение анкеты займёт около 4 минут, а решение принимается за 1 минуту. Деньги вы сможете получить в тот же день.</p></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	);
}

export default FAQ;