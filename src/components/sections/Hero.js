import React from 'react';
import { Link } from 'react-router-dom';
import Calculator from './Calculator';

const Hero = () => {
	return (
		<section id="hero">
		<header className="bump-bottom-md">
			<div className="container">
				<div className="row align-items-center">
					<div className="col-12 col-lg-2">
						<Link to="/" className="txt-white bold">
							Logo
						</Link>
					</div>
					<div className="col-3 col-lg-10 d-none d-lg-block">
						<nav>
							<ul>
								<li><a href="#how-it-works">Как получить деньги</a></li>
								<li><a href="#why">Преимущества</a></li>
								<li><a href="#faq">FAQ</a></li>
								<li><Link to="/register">Подать заявку</Link></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</header>

		<div className="container container-calculator">
			<div className="row">
				<div className="col-lg-7 col-xl-6 align-self-center">
					<div id="caption-slide">
						<div id="caption-box">
							<h1>Получите до <span className="no-wrap">100 000<small className="rub"> ₽</small></span><br />Срок от 3 до 12 месяцев</h1>
							<blockquote className="">
								<p>&laquo;Сравнивала разные компании. Проценты по сравнению с другими — отличные!&raquo;</p>
								<cite>- Юлия Кондратьева. Казань</cite>
							</blockquote>
						</div>
					</div>
				</div>
				<div className="col-lg-5 col-xl-4 offset-xl-2">
					<Calculator />
				</div>
			</div>
		</div>
	</section>
	);
}

export default Hero;