import { firstDate } from '../components/calculator/loanDate';
// let dueDate = new Date();
// dueDate.setTime(dueDate.getTime() + 14 * 24 * 60 * 60 * 1000);
// const firstDate = dueDate.getDate() + '.' + (dueDate.getMonth() + 1) + '.' + dueDate.getFullYear();

const initState = {
	loanAmount: 50000,
	loanTerm: 24,
	annualRate: 0,
	dailyRate: 0,
	periods: 0,
	rate: 0,
	fee: 0,
	repayment: 0,
	firstDate: firstDate
};

const loanReducer = (state = initState, action) => {
	switch (action.type) {
	case 'UPDATE_LOAN':
		return action.loan;
	default:
		return state;
	}
};

export default loanReducer;