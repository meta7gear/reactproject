import loanReducer from './loanReducer';
import { combineReducers } from 'redux';

const rootReducer = combineReducers({
  loan: loanReducer
});

export default rootReducer;